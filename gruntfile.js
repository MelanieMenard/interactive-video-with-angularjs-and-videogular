'use strict';

module.exports = function (grunt) {
    // Load all tasks
    require('load-grunt-tasks')(grunt);
    // Show elapsed time
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: {
            // Configurable paths
            app: 'app'
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            grunt: {
                files: ['gruntfile.js'],
                options: {
                    reload: true
                }
            },
            sass: {
                files: [
                    '<%= config.app %>/style/scss/*.scss'
                ],
                tasks: ['sass:development']
            },
            css: {
                files: [
                    '<%= config.app %>/style/css/*.css'
                ],
                options: {
                    reload: true
                }
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= config.app %>/{,*/}*.html',
                    '<%= config.app %>/style/css/*.css',
                    '<%= config.app %>/assets/{,*/}*',
                    '<%= config.app %>/data/*.json'
                ]
            }
        },

        connect: {
            options: {
                port: 3000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '<%= config.app %>'
                    ]
                }
            },
        },

        // Compile the scss source into a single css file.
        sass: {
            development: {
                files: {
                    '<%= config.app %>/style/css/main.css': '<%= config.app %>/style/scss/main.scss'
                },
                options: {
                    style: 'expanded',
                    lineNumbers: true
                }
            },
            dist: {
                files: {
                    '<%= config.app %>/style/css/main.css': '<%= config.app %>/style/scss/main.scss'
                },
                options: {
                    style: 'compressed'
                }
            }
        }

    });

    // Register tasks
    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {

        grunt.task.run([
            'sass:development',
            'connect:livereload',
            'watch'
        ]);
    });

};
