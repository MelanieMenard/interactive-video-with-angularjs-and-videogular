angular.module("interactiveVideoApp").factory("NarrativeStructure", ["$resource",
	function($resource){
		// store the narrative structure in an object 
		// so it's easier for scene controller to retrieve scene Data by sceneID
		// and we can also store different data structure for the menu and splash
		// if array, we could use the default query but for object we need to make a custom action
		return $resource("data/narrative_structure.json", {}, {
			query: {method:'GET', params:{}, isArray:false}
		});
	}]);