angular.module("interactiveVideoApp").config(["$stateProvider", "$locationProvider", "$urlRouterProvider",
function($stateProvider, $locationProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise("/splash");
	$locationProvider.html5Mode(false).hashPrefix('!');
	
    $stateProvider
	// MM: playOnEnter won't in practice be used with ui-sref for splash becasue we only load it, never access it though a link
	// but in the general case, an app with different scene structure that goes back to splash could use it
	.state("splash", {
		url: "/splash",
		params: {
			playOnEnter: true
		},
		templateUrl: "partials/splash.html",
		controller: "SplashController",
		controllerAs: "splashCtrl"
    })
	.state("menu", {
		url: "/menu",
		params: {
			playOnEnter: true
		},
		templateUrl: "partials/menu.html",
		controller: "MenuController",
		controllerAs: "menuCtrl"
    })
	// sceneID doesn't show on url (so user has no indication where they're moving to)
	// playOnEnter = true for initial visit of a sequence, false when you try again from a 'wrong outcome' (you've already seen the parent sequence the last time, don't play it again)
	// there probably is a bit of  design muddle because in scenes.json, playOnEnter is defined 
	// 1) in the scene data itself, which is used to create sref links on the menu. if the scene is not on menu, this parameter is useless
	// 2) overriden in branches, which is used when creating buttons from scene. This is useful because scene 1 should be played is accessed from menu, but not replayed is coming  back from child outcome
	// I think it's best to have possibly useless data, be aware of it, but we may need it for interactive videos where the scene structure is more complex
	// !!!! it means the scene controller should always read playOnEnter from $stateParams, NOTE sceneData
	// !!! because playOnEnter from sceneData may have been overriden by playOnEnter in branch link, depending on where we're coming from
	.state("scene", {
		url: "/scene",
		params: {
			sceneID: null,
			playOnEnter: false
		},
		templateUrl: "partials/scene.html",
		controller: "SceneController",
		controllerAs: "sceneCtrl"
    });
	  
 }]);