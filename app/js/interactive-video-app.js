'use strict';
angular.module("interactiveVideoApp",
	[
		"ngSanitize",
		"ngResource",
		"ngAnimate",
		"ui.router",
		"com.2fdevs.videogular",
		"com.2fdevs.videogular.plugins.controls",
		"com.2fdevs.videogular.plugins.overlayplay"
	]
)
.run(["$rootScope", "$state", "$stateParams",
	function ($rootScope, $state, $stateParams) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.
		// MM: I don't think we need to do that because all the controllers depend on the "$state" and "$stateParams" services
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }]);