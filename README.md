# Interactive video #

Standalone interactive video webapp made with AngularJS and [Videogular](https://github.com/videogular/videogular).

I overlayed a UI layer made with Angular views and router on top of a HTML5 video powered by Videogular. The UI layer controller retrieves the scene structure (with start and stop timings) from a JSON file via a custom service and displays custom UI for each scene offering the user to go to various other scenes. When the user clicks on a UI element, the router goes to another scene and the UI controller dispatches an event saying the scene has changed. The video controller catches the event, plays the video sequence associated to the new scene, and dispatch an event at the end of the sequence.

PLEASE NOTE THE LIVE DEMO IS TEMPORARILY DOWN until I can sort out cheap hosting fast enough to cope (ran out of amazon free trial). In the meantime please download the code and run it locally in node.js to see the working demo. Many thanks for your understanding for cost-conscious artists :)

Live demo now available (with a temporary video and random scenes, the creative side still needs to be done):

[http://www.melaniemenard.com/interactive-video/app/](http://www.melaniemenard.com/interactive-video/app/)

### How do I get set up? ###

* Install nodeJS if you don't already have it and run 'npm install' in command line, it will install all the dependencies for you.
* create a 'media' folder in /app and put a video in it, or link one from the web (in app/js/interactive-video-controller.js -> find the config, you'll find examples of both ways to link videos)
* write the narrative structures in data/narrative structure.json (sample scenes provided so you know how to format the data)
* Run the app!

### Contribution guidelines ###

* This is private code for a digital art project.

### Who do I talk to? ###

* contact@melaniemenard.com