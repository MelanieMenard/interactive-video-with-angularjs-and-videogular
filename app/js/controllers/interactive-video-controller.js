angular.module("interactiveVideoApp").controller("InteractiveVideoController", ["$rootScope", "$scope", "$sce", "$stateParams", "$state",  function ($rootScope, $scope, $sce, $stateParams, $state) {
		
		var controller = this;
		
		controller.API = null;
	
		controller.lastLoggedTime = 0;
        controller.totalTime = 0;
        controller.state = null;
        controller.volume = 1;
        controller.isCompleted = false;
		
		// these are used by videogular when looking for the start of a sequence
		controller.seeking = {
            currentTime: 0,
            duration: 0
        };
        controller.seeked = {
            currentTime: 0,
            duration: 0
        };
		
		// null if no sequence currently playing
		// parameters fr the currently playing sequence: {startTime, endTime, loop: true/false}
		controller.currSequence = null;		

		
		// 'play-sequence' is triggered via $rootScope.$broadcast in the scene controller
		// !!! events must be sent by $rootScope.$broadcast but caught by $scope.$on in the other controller
		// !!! if you catch them in a controller with $rootScope.$on, it gets recreated every time you make the controller but never destroyed, which causes:
		// !!! 1) memory leak 2) same event caught multiple times
		$scope.$on('play-sequence', function(e, args){
			console.log('Catch play-sequence in videoCtrl. startTime: '+args.startTime+', endTime: '+args.endTime);
			controller.currSequence = {};
			controller.currSequence.startTime = args.startTime;
			controller.currSequence.endTime = args.endTime;
			controller.currSequence.loop = args.loop;
			controller.playFrom(controller.currSequence.startTime);
		});
		
		// 'display-freeze-frame' is triggered via $rootScope.$broadcast in the scene controller
		$scope.$on('display-freeze-frame', function(e, args){
			console.log('Catch display-freeze-frame in videoCtrl. frameTime: '+args.frameTime);
			controller.displayFrame(args.frameTime);
		});
		
		controller.config = {
			autoPlay: false,
			/* in the real app, the video will ideally be embedded - it now works as dev with latest http server */
			/*"preload": "preload",
			sources: [
				{src: $sce.trustAsResourceUrl("media/GHsilent.mp4"), type: "video/mp4"},
				{src: $sce.trustAsResourceUrl("media/GHsilent.webm"), type: "video/webm"}
			],*/
			/* however, depending on how the server responds where the app is hosted, we may need to host the video externally 
			   because videogular relies on the server being able to returbn chunks on the video file from specific times
			   standard hosting too slow, video stalls, but amazon S3 works fine
			*/
			/*"preload": "preload",
			sources: [
				{src: $sce.trustAsResourceUrl("http://www.melaniemenard.com/interactive-video/app/media/GHsilent.mp4"), type: "video/mp4"}
				{src: $sce.trustAsResourceUrl("http://www.melaniemenard.com/interactive-video/app/media/GHsilent.webm"), type: "video/webm"}
			],*/
			"preload": "auto",
			sources: [
				{src: $sce.trustAsResourceUrl("https://s3-eu-west-1.amazonaws.com/melaniemenard/interactivevideo/GHsilent.mp4"), type: "video/mp4"}
			],
			tracks: [
			],
			theme: "bower_components/videogular-themes-default/videogular.css",
			plugins: {
			}
		};
		
		// default event fired by Videogular
		controller.onPlayerReady = function (API) {
			console.log('onPlayerReady triggered in video controller');
			controller.API = API;
        };
		
		// custom Vidogular API wrapper: play a sequence from specified start point
		controller.playFrom = function(time){
			if (controller.API) {
				console.log('videoController.playFrom: '+time);
				controller.API.seekTime(time, false);
				controller.API.play();
			}
			else {
				console.log('videoController.playFrom ERROR: cant find reference to API');
			}			
		};
		
		// custom Vidogular API wrapper: display a freeze frame at specified time without playing the video
		controller.displayFrame = function(time){
			if (controller.API) {
				console.log('videoController.displayFrame: '+time);
				controller.API.seekTime(time, false);
			}
			else {
				console.log('videoController.displayFrame ERROR: cant find reference to API');
			}			
		};

		// custom event for interactive video: notifies the endTime of the currently playing sequence has passed
		controller.onEndSequenceReached = function(reachedTime){
		
			console.log('videoCtrl.onEndSequenceReached: '+controller.currSequence.endTime+', previous time: '+controller.lastLoggedTime+', current time: '+reachedTime);
			
			// Case 1: loop forever
			if (controller.currSequence.loop) {
				controller.playFrom(controller.currSequence.startTime);
			}
			// Case 2: pause and notify the UI view
			else {
				$rootScope.$broadcast('video-sequence-end-reached', {sequenceEndTime: controller.currSequence.endTime, actualTimeReached: reachedTime});			
				if (controller.API) {
					controller.API.pause();
				}
				else {
					console.log('videoController.playFrom ERROR: cant find reference to API');
				}	
				controller.currSequence = null;
			}
		};
		
		// default event fired by Videogular
		controller.onError = function (event) {
            console.log("VIDEOGULAR ERROR EVENT");
            console.log(event);
        };

		// default event fired by Videogular
        controller.onCompleteVideo = function () {
            controller.isCompleted = true;
        };

		// default event fired by Videogular
        controller.onUpdateState = function (state) {
            controller.state = state;
			
			// this is used within the scene view so should be put on $rootScope
			if (state === 'play') {
				if (!$rootScope.playerState) {
					$rootScope.playerState = {};
				}
				$rootScope.playerState.isPlaying = true;
				console.log('$rootScope.playerState.isPlaying: '+$rootScope.playerState.isPlaying)
			}
			else {
				if (!$rootScope.playerState) {
					$rootScope.playerState = {};
				}
				$rootScope.playerState.isPlaying = false;
				console.log('$rootScope.playerState.isPlaying: '+$rootScope.playerState.isPlaying)
			}
			console.log('video state: '+state);
        };

		// default event fired by Videogular
        controller.onUpdateTime = function (currentTime, totalTime) {
            			
			// MM: custom built to watch for the end of the sequence currently playing
			if (controller.currSequence !== null) {
				if (controller.lastLoggedTime < controller.currSequence.endTime && currentTime >= controller.currSequence.endTime) {				
					controller.onEndSequenceReached(currentTime);					
				}
			}
			
			// MM: update controller.lastLoggedTime AFTER doing the end sequence check
			controller.lastLoggedTime = currentTime;
            controller.totalTime = totalTime;
        };

        // default event fired by Videogular
		controller.onSeeking = function (currentTime, duration) {
            controller.seeking.currentTime = currentTime;
            controller.seeking.duration = duration;
			console.log('videoCtrl.onSeeking: '+currentTime);
        };

        // default event fired by Videogular
		controller.onSeeked = function (currentTime, duration) {
            controller.seeked.currentTime = currentTime;
            controller.seeked.duration = duration;
			console.log('videoCtrl.onSeeked: '+currentTime);
        };

        // default event fired by Videogular
		controller.onUpdateVolume = function (newVol) {
            controller.volume = newVol;
        };
		
	}]);