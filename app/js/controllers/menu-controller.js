angular.module("interactiveVideoApp").controller("MenuController", ["$rootScope", "$scope", "$stateParams","$state", "NarrativeStructure", function ($rootScope, $scope, $stateParams, $state, NarrativeStructure) {

		var controller = this;		
		
		// playOnEnter = true for initial visit of the menu from splash
		// false or false when you get back to menu from the 'good outcome' of a scene
		// if no background loop for the menu, just set to false and write the same time for startTime and endTime in the json
		controller.playOnEnter = $stateParams.playOnEnter;
				
		controller.scenes = null;
		controller.menuData = null;
		
		
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		
			// MM: on app load, state is empty
			var oldState = fromState.name ? fromState.name : "initial state empty";
			console.log('$stateChangeSuccess caught in menuCtrl. old state: '+oldState+', new state: '+toState.name);
			
			// get the narrative structure from json file
			// MM: the structure must be on $rootScope so visited state persists across all views
			if ($rootScope.structure) {
				controller.initData();
				controller.setInitialVideoState();
			}
			else {
				$rootScope.structure = NarrativeStructure.query(function(structure) {
					console.log('JSON callback for menu');
					controller.initData();
					controller.setInitialVideoState();
				});
			}			
		});
		
		
		// MM: just to have the $viewContentLoaded event ready in case we need to move stuff from $stateChangeSuccess to here
		// MM: we probably won't need it because $stateChangeSuccess seems to always happen after $viewContentLoaded in Chrome console
		$scope.$on('$viewContentLoaded', function(event){ 
			console.log('$viewContentLoaded caught in menuCtrl. state: '+$state.current.name);		
		});
		
		
		// custom event fired by video controller when the end of the sequence is reached
		// in this app, the menu sequence loops forever until we exit the state, so this will never get triggered
		// if you want to play the menu sequence once, then do something, this is where you put the code
		$scope.$on('video-sequence-end-reached', function(e, args){
			console.log('Catch video-sequence-end-reached in menuCtrl. sequenceEndTime: '+args.sequenceEndTime+', actualTimeReached: '+args.actualTimeReached);			
			// insert code to do something to the UI when the sequence has finished playing			
		});
	
		
		// initialise data from JSON
		controller.initData = function(){
			controller.scenes = $rootScope.structure.scenes;
			controller.menuData = $rootScope.structure.menu;	
		};
		
		
		// set initial video state
		controller.setInitialVideoState = function(){
		
			// Case 1: play a background video loop underneath the UI
			if (controller.playOnEnter) {
			
				// play-sequence event will be caught by the videoController to play the sequence			
				console.log('Args for play-sequence: start: '+controller.menuData.startTime+', end: '+controller.menuData.endTime);
				$rootScope.$broadcast('play-sequence', {startTime: controller.menuData.startTime, endTime: controller.menuData.endTime, loop: controller.menuData.loopSequence});
				
				// insert code to do something to the UI while the video plays
			}
			// Case 2: show a freeze frame (use startTime)
			else {
				$rootScope.$broadcast('display-freeze-frame', {frameTime: controller.menuData.startTime});
			}
		};
		
	}]);