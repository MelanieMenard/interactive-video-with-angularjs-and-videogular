angular.module("interactiveVideoApp").controller("SceneController", ["$rootScope", "$scope", "$stateParams","$state", "NarrativeStructure",  function ($rootScope, $scope, $stateParams, $state, NarrativeStructure) {

		var controller = this;
		
		// get the sceneID from router parameters
		controller.sceneID = $stateParams.sceneID;
		
		// playOnEnter = true for initial visit of a sequence, false when you try again from a 'wrong outcome' (you've already seen the parent sequence the last time, don't play it again)
		// there probably is a bit of  design muddle because in scenes.json, playOnEnter is defined 
		// 1) in the scene data itself, which is used to create sref links on the menu. if the scene is not on menu, this parameter is useless
		// 2) overriden in branches, which is used when creating buttons from scene. This is useful because scene 1 should be played is accessed from menu, but not replayed is coming  back from child outcome
		// I think it's best to have possibly useless data, be aware of it, but we may need it for interactive videos where the scene structure is more complex
		// !!!! it means the scene controller should always read playOnEnter from $stateParams, NOTE sceneData
		// !!! because playOnEnter from sceneData may have been overriden by playOnEnter in branch link, depending on where we're coming from
		controller.playOnEnter = $stateParams.playOnEnter;
				
		controller.scenes = null;
		controller.sceneData = null;
		
		// hide views when video is playing
		controller.playerState = $rootScope.playerState;
		
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		
			// MM: on app load, state is empty
			var oldState = fromState.name ? fromState.name : "initial state empty";
			var oldSceneID = fromParams.sceneID ? fromParams.sceneID : "no old SceneID";
			var newSceneID = toParams.sceneID ? toParams.sceneID : "no new SceneID";
			console.log('$stateChangeSuccess caught in sceneCtrl. old state: '+oldState+' '+oldSceneID+', new state: '+toState.name+' '+newSceneID);
			
			// get the narrative structure from json file, and when we get the data back, store the data related to this particular scene
			// MM: the structure must be on $rootScope so visited state persists across all views
			if ($rootScope.structure) {
				controller.initData();
				controller.setInitialVideoState();
			}
			else {
				$rootScope.structure = NarrativeStructure.query(function(structure) {
					console.log('JSON callback for: '+controller.sceneID);
					controller.initData();
					controller.setInitialVideoState();
				});
			}
			
		});
		
		
		// MM: just to have the $viewContentLoaded event ready in case we need to move stuff from $stateChangeSuccess to here
		// MM: we probably won't need it because $stateChangeSuccess seems to always happen after $viewContentLoaded in Chrome console
		$scope.$on('$viewContentLoaded', function(event){ 
			console.log('$viewContentLoaded caught in menuCtrl. state: '+$state.current.name+' '+$stateParams.sceneID);		
		});
		
		
		// custom event fired by video controller
		$scope.$on('video-sequence-end-reached', function(e, args){
			console.log('Catch video-sequence-end-reached in sceneCtrl. sequenceEndTime: '+args.sequenceEndTime+', actualTimeReached: '+args.actualTimeReached);
			
			if (controller.playOnEnter) {
				// TODO: here fade in UI (for the moment is hows/hide instantly with ng-hide = videoCtrl.isPlaying)
			}
			console.log('video-sequence-end-reached in sceneCtrl. controller.isVideoPlaying: '+controller.isVideoPlaying);
			
		});
		
		
		// initialise data from JSON
		controller.initData = function(){		
			controller.scenes = $rootScope.structure.scenes;
			controller.sceneData = controller.scenes[controller.sceneID];
			// mark scene as visited
			controller.scenes[controller.sceneID].isVisited = true;
		};
		
		
		// set initial video state
		controller.setInitialVideoState = function(){
		
			// Case 1: play the scene video sequence before displaying the question
			if (controller.playOnEnter) {
			
				// play-sequence event will be caught by the videoController to play the sequence			
				console.log('Args for play-sequence: start: '+controller.sceneData.startTime+', end: '+controller.sceneData.endTime);
				$rootScope.$broadcast('play-sequence', {startTime: controller.sceneData.startTime, endTime: controller.sceneData.endTime, loop: false});
			
				// TODO: here fade out UI (for the moment is hows/hide instantly with ng-hide = videoCtrl.isPlaying)
			}
			// Case 2: only display the last freeze frame of the sequence underneath the question (when you go back to the scene from a wrong outcome)
			else {
				$rootScope.$broadcast('display-freeze-frame', {frameTime: controller.sceneData.endTime});
			}
		};
		
		
		// Here put everything that needs to happen when the user clicks on a branch link
		controller.onBranchClick = function(branchID){
		
			// mark the link as visited when a user clicks on one of scene branches
			for (var i=0; i<controller.sceneData.branches.length; i++) {
				if (controller.sceneData.branches[i].id === branchID) {
					controller.sceneData.branches[i].isVisited = true;
					break;
				}
			}
			// the scene associated to the branch will be marked as visited on state change success
			// isVisited is present both at branchLink level and in the scene data proper as 2 separate variables
			// the rationale for double variable is that in complex scenario, the same scene can be visited through different paths
			// and you may want to track separately whether the user has seen the scene altogether, or via this particular path
			// I think how useful/sensible this double variable is depends on scenario complexity, and therefore is subject to change	
		};
		
	}]);